odoo.define('machine_repair_management_extend.machine_repair', function (require) {
"use strict";
 
require('web.dom_ready');
var ajax = require('web.ajax');

// Show hides Fileds
$("#Equipment_Information").hide();
$("#Location_Name").hide();

$("select[name='contract_project_id']").on("change", function (){
    if ($("select[name='contract_project_id']")[0].value != ''){
        $("#Location_Name").show();
        $("#Equipment_Information").show();
    }
});

$("select[name='custom_location_id']").on("change", function (){
        var partner=$(this).val();
        var location_address = $(this).children("option:selected").attr('data-location-address')
        // $("select[name='address_id']").find("option[value="+location_address+"]").attr("selected","selected")
     	$("#location_address").val(location_address)
       });

var route_options = $("select[name='product_id']:enabled option:not(:first)");
var route= $("select[name='custom_location_id']:enabled option:not(:first)");
$("select[name='contract_project_id']").on("change", function (){
        var project=$(this).val();
        var contract_number = $(this).children("option:selected").attr('data-analytic-account')
        $("select[name='contract_number']").find("option[value="+contract_number+"]").attr("selected","selected")
        $("#contract_number").val(contract_number)
        var select = $("select[name='product_id']");
        route_options.detach();
        var displayed_routes = route_options.filter("[data-project-id="+($(this).val() || 0)+"]");
        var nb = displayed_routes.appendTo(select).show().size();
        select.parent().toggle(nb>=0);
     
        var contract = $(this).children("option:selected").attr('data-partner')
        var selected = $("select[name='custom_location_id']");
        route.detach();
        var displayed = route.filter("[data-partner="+(contract || 0)+"]");
        var partner = displayed.appendTo(selected).show().size();
        selected.parent().toggle(partner>=0);
 });

var route_options = $("select[name='product_id']:enabled option:not(:first)");
$("select[name='custom_location_id']").on("change", function (){
        var select = $("select[name='product_id']");
        route_options.detach();
        var displayed_routes = route_options.filter("[data-child-id="+($(this).val() || 0)+"]");
        var nb = displayed_routes.appendTo(select).show().size();
        select.parent().toggle(nb>=0);
        });


$("select[name='product_id']").on("change", function (){
        var product=$(this).val();
        var equipment_barcode = $(this).children("option:selected").attr('data-barcode')
        // $("select[name='company_serial_number']").find("option[value="+equipment_barcode+"]").attr("selected","selected")
        $("#company_serial_number").val(equipment_barcode)
       });

});

