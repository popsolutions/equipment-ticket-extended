# -*- coding: utf-8 -*-

import base64
from odoo import http, _
from odoo.http import request
from odoo import models,registry, SUPERUSER_ID
from odoo.addons.machine_repair_management.controllers.main import MachineRepairSupport

class MachineRepairSupport(MachineRepairSupport):

    @http.route(['/page/machine_repair_support_ticket'], type='http', auth="public", website=True)
    def open_machine_repair(self, **post):
        partner_obj = request.env['res.partner'].sudo()
        service_ids = request.env['service.nature'].sudo().search([])
        srvice_type_ids = request.env['repair.type'].sudo().search([])
        project_obj = request.env['project.project'].sudo()
        product_obj = request.env['product.product'].sudo()

        partner_id = request.env.user.partner_id
        if partner_id.parent_id:
            partner_id = partner_id.parent_id

        partner_ids = partner_obj.search([])
        domain = [('partner_id', 'child_of', partner_id.ids)]

        contract_project_ids = project_obj.search(domain)
        location_ids = partner_ids


        product_ids = product_obj.search([])
        return request.render("machine_repair_management.website_machine_repair_support_ticket", {
            'service_ids': service_ids,
            'srvice_type_ids': srvice_type_ids,
            'contract_project_ids':contract_project_ids,
            'location_ids':location_ids,
            'product_ids':product_ids,
        })

    @http.route(['/machine_repair_management/request_submitted'], type='http', auth="public", methods=['POST'], website=True)
    def request_submitted(self, **post):
        Partner = request.env['res.partner'].sudo().search([('email', '=', post['email'])])
        if Partner:
            team_obj = request.env['machine.support.team']
            team_match = team_obj.sudo().search([('is_team','=', True)], limit=1)
            support = request.env['machine.repair.support'].sudo().create({
                'subject': post['subject'],
                'team_id' :team_match.id,
                'partner_id' :team_match.leader_id.id,
                'user_id' :team_match.leader_id.id,
                'email': post['email'],
                'phone': post['phone'],
                'description': post['description'],
                'priority': post['priority'],
                'partner_id': Partner.id,
                'website_brand': post['brand'],
                'website_model': post['model'],
                'damage': post['damage'],
                'website_year': post['year'],
                'nature_of_service_id': int(post['service_id']),
                'project_id':post['contract_project_id'],
                'contract_number':post['contract_number'],
                'custom_location_id':post['custom_location_id'],
                'product_id':post['product_id'],
                'company_serial_number':post['company_serial_number'],
                 })
            values = {
                'support':support,
                'user':request.env.user
            }

            attachment_list = request.httprequest.files.getlist('attachment')
            for image in attachment_list:
                if post.get('attachment'):
                    attachments = {
                               'res_name': image.filename,
                               'res_model': 'machine.repair.support',
                               'res_id': support,
                               'datas': base64.encodestring(image.read()),
                               'type': 'binary',
                               'datas_fname': image.filename,
                               'name': image.filename,
                           }
                    attachment_obj = http.request.env['ir.attachment']
                    attach = attachment_obj.sudo().create(attachments)
            if len(attachment_list) > 0:
                group_msg = _('Customer has sent %s attachments to this machine repair ticket. Name of attachments are: ') % (len(attachment_list))
                for attach in attachment_list:
                    group_msg = group_msg + '\n' + attach.filename
                group_msg = group_msg + '\n'  +  '. You can see top attachment menu to download attachments.'
                support.sudo().message_post(body=group_msg,message_type='comment')
            return request.render('machine_repair_management.thanks_mail_send', values)
        else:
            return request.render('machine_repair_management.support_invalid',{'user':request.env.user})