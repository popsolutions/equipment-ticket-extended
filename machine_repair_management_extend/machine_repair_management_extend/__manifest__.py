# -*- coding: utf-8 -*-

# Part of Probuse Consulting Service Pvt Ltd. See LICENSE file for full copyright and licensing details.

{
    'name': "Machine Repair Management Extend(All Types of Machines)",
    'price': 0.0,
    'currency': 'EUR',
    'license': 'Other proprietary',
    'summary': """This module develop to full fill requirements of Machine Repair Services Provider or Industry.""",
    'description': """
Machine Repair Request and Management
This module develop to full fill requirements of Machine Repair Services Provider or Industry.
    """,
    'author': "Probuse Consulting Service Pvt. Ltd.",
    'website': "http://www.probuse.com",
    'support': 'contact@probuse.com',
    'version': '1.1.2',
    'category' : 'Manufacturing',
    'depends': [
        'machine_repair_management'
    ],
    'data':[
        'views/project_template.xml',
        'views/machine_repair_portal.xml',
        'views/machine_repair_request.xml',
        'views/partner.xml',
        'reports/support_request.xml',
    ],
    'installable' : True,
    'application' : False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
