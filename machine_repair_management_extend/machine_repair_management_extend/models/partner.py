# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class ResPartner(models.Model):
    _inherit = "res.partner"

    @api.multi
    def action_button_partner(self):
        self.ensure_one()
        action = self.env.ref("project.open_view_project_all_config").read([])[0]
        action['domain'] = [('partner_id', 'child_of',self.id)]
        return action
