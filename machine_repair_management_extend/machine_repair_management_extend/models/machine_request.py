# -*- coding: utf-8 -*

from odoo import models, fields, api

class ProductProduct(models.Model):
    _inherit = 'machine.repair.support'

    contract_number = fields.Char(
        string='Contract Number',
    )

    custom_location_id = fields.Many2one(
        'res.partner', 
        string='Location Name', 
    )
    company_serial_number = fields.Char(
        string='Company Serial Number',
    )

    @api.onchange('project_id')
    def onchange_project_id(self):
        self.ensure_one()
        res = {}
        self.contract_number = self.project_id.analytic_account_id.name
        parttner_ids = []
        if self.project_id and self.project_id.partner_id:
            parttner_ids.append(self.project_id.partner_id.id)
        res['domain'] = {
            'custom_location_id': [('id', 'child_of', parttner_ids)],
        }
        return res

    @api.onchange('product_id')
    def onchange_product_id(self):
        self.ensure_one()
        self.company_serial_number = self.product_id.barcode
