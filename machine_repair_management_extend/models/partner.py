# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class ResPartner(models.Model):
    _inherit = "res.partner"

    @api.multi
    def action_button_partner(self):
        self.ensure_one()
        action = self.env.ref("project.open_view_project_all_config").read([])[0]
        action['domain'] = [('partner_id', 'child_of',self.id)]
        return action

#    @api.model
#    def create(self, vals):
#        res = super(ResPartner, self).create(vals)
#        # Below code for location child
#        if res and res.parent_id and res.parent_id.type  == 'other':
#            partner_ids = res.parent_id.child_ids
#            if res.parent_id.parent_id:
#                partner_ids += res.parent_id.parent_id
#            ticket_ids = self.env['machine.repair.support'].sudo().search([
#                ('partner_id', 'in', partner_ids.ids)
#            ])
#            for ticket_id in ticket_ids:
#                ticket_id._compute_allow_contact_ids()

#        # Below code for contract child
#        if res and res.parent_id and res.type  == 'contact':
#            if any(child.type == 'other' for child in res.parent_id.child_ids):
#                query1 = """
#                    WITH RECURSIVE children AS (
#                    SELECT id
#                    FROM res_partner
#                    WHERE parent_id = %s
#                    UNION ALL
#                    SELECT a.id
#                    FROM res_partner a
#                    JOIN children b ON(a.parent_id = b.id)   
#                    )
#                    SELECT * FROM children
#                """
#                self._cr.execute(query1 % (res.parent_id.id))
#                direct_child_ids_res = self._cr.dictfetchall()
#                child_ids = [i['id'] for i in direct_child_ids_res]
#                ticket_ids = self.env['machine.repair.support'].sudo().search([
#                    ('partner_id', 'in', child_ids)
#                ])
#                for ticket_id in ticket_ids:
#                    ticket_id._compute_allow_contact_ids()
#        return res

