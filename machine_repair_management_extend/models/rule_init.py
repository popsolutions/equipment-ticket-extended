# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class IrRule(models.Model):
    _inherit = "ir.rule"

    @api.model_cr
    def init(self):
        record_rule_id = self.env.ref('machine_repair_management.machine_repair_portal_rule')
        record_rule_id.domain_force = "[('allow_contact_ids','in', user.partner_id.id)]"
