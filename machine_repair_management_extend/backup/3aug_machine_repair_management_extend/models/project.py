# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class ProjectProject(models.Model):
    _inherit = "project.project"

    contract_product_ids = fields.One2many(
        'product.product',
        'contract_project_id',
        string="Equipment"
    )

class ProjectTask(models.Model):
    _inherit = "project.task"

    custom_booked_date = fields.Date(
        string="Visit Date"
    )
    is_book_maintenance = fields.Boolean(
        string="Is Book Maintenance",
        copy=True
    )
