# -*- coding: utf-8 -*

from odoo import models, fields, api

class ProductTemplate(models.Model):
    _inherit = 'product.template'
    
    contract_project_id = fields.Many2one(
        'project.project', 
        string='Contract Name',
        copy = False
    )
    contract_number = fields.Char(
        string='Contract Number',
        copy = False
    )
    custom_location_id = fields.Many2one(
        'res.partner', 
        string='Location Name',
        copy = False
    )

    @api.onchange('contract_project_id')
    def onchange_contract_project_id(self):
        res = {}
        self.contract_number = self.contract_project_id.analytic_account_id.name
        parttner_ids = []
        if self.contract_project_id and self.contract_project_id.partner_id:
            parttner_ids.append(self.contract_project_id.partner_id.id)
        res.update({
            'domain': {
                'custom_location_id': [('id', 'child_of', parttner_ids), ('type', '=', 'other')]
            }
        })
        return res

class ProductProduct(models.Model):
    _inherit = 'product.product'
    
    @api.onchange('contract_project_id')
    def onchange_contract_project_id(self):
        res = {}
        self.contract_number = self.contract_project_id.analytic_account_id.name
        parttner_ids = []
        if self.contract_project_id and self.contract_project_id.partner_id:
            parttner_ids.append(self.contract_project_id.partner_id.id)
        res.update({
            'domain': {
                'custom_location_id': [('id', 'child_of', parttner_ids), ('type', '=', 'other')]
            }
        })
        return res

