odoo.define('machine_repair_management_extend.machine_repair', function (require) {
"use strict";
 
    require('web.dom_ready');
    var ajax = require('web.ajax');

    // Trigger selection fields
    $(document).ready(function(){
        $("select[name='contract_project_id']").trigger('change');
        $("select[name='machine_service']").trigger('change');
        $("select[name='custom_location_id']").trigger('change');
        $("select[name='product_id']").trigger('change');
    });

    // Condition based required selection fields
    $("#ContractSection").show();
    $("#contract_project_id").prop('required',true);
    $("#contract_number").prop('required',true);
    $("#custom_location_id").prop('required',true);
    $("#location_address").prop('required',true);
    $("#product_id").prop('required',true);
    $("#company_serial_number").prop('required',true);
    $('#isContractUserSelected').click(function() {
        if (this.checked){
            $("#ContractSection").show();
            $("#ContractSection").toggle(this.checked);
            $("#contract_project_id").prop('required',true);
            $("#contract_number").prop('required',true);
            $("#custom_location_id").prop('required',true);
            $("#location_address").prop('required',true);
            $("#product_id").prop('required',true);
            $("#company_serial_number").prop('required',true);
        } 
        else{
            $("#ContractSection").hide();
            $("#ContractSection").toggle(this.checked);
            $("#contract_project_id").prop('required',false);
            $("#contract_number").prop('required',false);
            $("#custom_location_id").prop('required',false);
            $("#location_address").prop('required',false);
            $("#product_id").prop('required',false);
            $("#company_serial_number").prop('required',false);
        }
    });


    // Show hides Fileds
    $("#Equipment_Information").hide();
    $("#Location_Name").hide();

    $("select[name='contract_project_id']").on("change", function (){
        if ($("select[name='contract_project_id']")[0].value != ''){
            $("#Location_Name").show();
            $("#Equipment_Information").show();
        }
    });

    // Set a location address for selected location base
    $("select[name='custom_location_id']").on("change", function (){
        var partner=$(this).val();
        var location_address = $(this).children("option:selected").attr('data-location-address')
        // $("select[name='address_id']").find("option[value="+location_address+"]").attr("selected","selected")
        $("#location_address").val(location_address)
    });

    // Filter a product and location based for condition
    var route_options = $("select[name='product_id']:enabled option:not(:first)");
    var route= $("select[name='custom_location_id']:enabled option:not(:first)");
    $("select[name='contract_project_id']").on("change", function (){
        var project=$(this).val();
        var contract_number = $(this).children("option:selected").attr('data-analytic-account')
        $("select[name='contract_number']").find("option[value="+contract_number+"]").attr("selected","selected")
        $("#contract_number").val(contract_number)
        var select = $("select[name='product_id']");
        route_options.detach();
        var displayed_routes = route_options.filter("[data-project-id="+($(this).val() || 0)+"]");
        var displayed_routes = displayed_routes.filter("[data-child-id="+($("#custom_location_id").val() || 0)+"]");
        var nb = displayed_routes.appendTo(select).show().size();
        select.parent().toggle(nb>=0);
        var contract = $(this).children("option:selected").attr('data-partner')
        var selected = $("select[name='custom_location_id']");
        route.detach();
        var displayed = route.filter("[data-partner="+(contract || 0)+"]");
        var partner = displayed.appendTo(selected).show().size();
        selected.parent().toggle(partner>=0);
     });

    // Filter a product
    var route_options = $("select[name='product_id']:enabled option:not(:first)");
    $("select[name='custom_location_id']").on("change", function (){
        var select = $("select[name='product_id']");
        route_options.detach();
        var displayed_routes = route_options.filter("[data-project-id="+($("#contract_project_id").val() || 0)+"]");
        var displayed_routes = displayed_routes.filter("[data-child-id="+($(this).val() || 0)+"]");
        var nb = displayed_routes.appendTo(select).show().size();
        select.parent().toggle(nb>=0);
    });

    // Set a Brand, Model and year for selected product related
    $("select[name='product_id']").on("change", function (){
        var product=$(this).val();
        var equipment_barcode = $(this).children("option:selected").attr('data-barcode')
        // $("select[name='company_serial_number']").find("option[value="+equipment_barcode+"]").attr("selected","selected")
        $("#company_serial_number").val(equipment_barcode)
         ajax.jsonRpc("/machine_repair_management/update_product_info", 'call', {
              'dict': {'product_id': product},
         }).then(function (data) {
              $("#brand").val(data['brand'])
              $("#model").val(data['model'])
              $("#year").val(data['year'])
         });
    });

    // Date picker
    $("div.input-group span.fa-calendar").on('click', function(e) {
        $(e.currentTarget).closest("div.date").datetimepicker({
            icons : {
                time: 'fa fa-clock-o',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
            },
            minDate : moment()
        });
    });

    // Show hide fields
    $("select[name='machine_service']").on("change", function (){
        if ($(this).val() == 'booked_type'){
            $("#booked_date").show();
            $("#txtdate").prop('required',true);
        }
        else{
            $("#booked_date").hide();
            $("#txtdate").prop('required',false);
        }
    });

});

