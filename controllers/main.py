# -*- coding: utf-8 -*-

import base64
from datetime import datetime
from odoo import http, _
from odoo.http import request
from odoo import models,registry, SUPERUSER_ID
from odoo.addons.machine_repair_management.controllers.main import MachineRepairSupport
from odoo.addons.machine_repair_management.controllers.main import website_account


class website_account(website_account):

    @http.route(['/my/repair_requests', '/my/repair_requests/page/<int:page>'], type='http', auth="user", website=True)
    def portal_my_repair_request(self, page=1, **kw):
        response = super(website_account, self)
        values = self._prepare_portal_layout_values()
        partner = request.env.user.partner_id
        support_obj = http.request.env['machine.repair.support']
        domain = [
            ('partner_id', 'child_of', [partner.commercial_partner_id.id])
        ]
        # pager
        pager = request.website.pager(
            url="/my/repair_requests",
            total=values.get('repair_request_count'),
            page=page,
            step=self._items_per_page
        )
        # content according to pager and archive selected
        repair_request = support_obj.search(domain, limit=self._items_per_page, offset=pager['offset'])
        values.update({
            'repair_requests': repair_request,
            'page_name': 'repair_requests',
            'pager': pager,
            'default_url': '/my/repair_requests',
        })
        return request.render("machine_repair_management.display_repair_requests", values)

class MachineRepairSupport(MachineRepairSupport):

    @http.route(['/page/machine_repair_support_ticket'], type='http', auth="public", website=True)
    def open_machine_repair(self, **post):
        partner_obj = request.env['res.partner'].sudo()
        service_ids = request.env['service.nature'].sudo().search([])
        srvice_type_ids = request.env['repair.type'].sudo().search([])
        project_obj = request.env['project.project'].sudo()
        product_obj = request.env['product.product'].sudo()

        partner_id = request.env.user.partner_id
        parent_ids = [partner_id.id]
        if partner_id.parent_id:
            while partner_id.parent_id:
                parent_ids.append(partner_id.parent_id.id)
                partner_id = partner_id.parent_id

#        partner_ids = partner_obj.search([('type', '=', 'other'), ('child_ids', 'in', request.env.user.partner_id.ids)]) # probuse 2 august, 2019

        if request.env.user.partner_id.parent_id.type == 'other':
            partner_ids = partner_obj.search([('type', '=', 'other'), ('child_ids', 'in', request.env.user.partner_id.ids)])
        elif request.env.user.partner_id.parent_id.company_type == 'company':
            partner_ids = request.env.user.partner_id.parent_id.child_ids.filtered(lambda m : m.type == 'other')
        elif not request.env.user.partner_id.parent_id and request.env.user.partner_id.company_type == 'company':
            partner_ids = request.env.user.partner_id.child_ids.filtered(lambda m : m.type == 'other')

        domain = [('partner_id', 'child_of', parent_ids)]

        contract_project_ids = project_obj.search(domain)
        location_ids = partner_ids

        product_ids = product_obj.search([])
        return request.render("machine_repair_management.website_machine_repair_support_ticket", {
            'service_ids': service_ids,
            'srvice_type_ids': srvice_type_ids,
            'contract_project_ids':contract_project_ids,
            'location_ids':location_ids,
            'product_ids':product_ids,
        })

    @http.route(['/machine_repair_management/request_submitted'], type='http', auth="public", methods=['POST'], website=True)
    def request_submitted(self, **post):
        Partner = request.env['res.partner'].sudo().search([('email', '=', post['email'])], limit=1)
        if Partner:
            team_obj = request.env['machine.support.team']
            team_match = team_obj.sudo().search([('is_team','=', True)], limit=1)
            support = request.env['machine.repair.support'].sudo().create({
                'subject': post['subject'],
                'team_id' :team_match.id,
                'partner_id' :team_match.leader_id.id,
                'user_id' :team_match.leader_id.id,
                'email': post['email'],
                'phone': post['phone'],
                'description': post['description'],
                'priority': post['priority'],
                'partner_id': Partner.id,
                'website_brand': post['brand'],
                'website_model': post['model'],
                'damage': post['damage'],
                'website_year': post['year'],
                'nature_of_service_id': int(post['service_id']),
                'project_id':post['contract_project_id'],
                'contract_number':post['contract_number'],
                'custom_location_id':post['custom_location_id'],
                'product_id':post['product_id'],
                'company_serial_number':post['company_serial_number'],
                'custom_machine_type': post['machine_service'] or 'repair_type',
                'custome_client_user_id': request.env.user.id
                 })
            values = {
                'support':support,
                'user':request.env.user
            }

            if support and support.custom_machine_type == 'booked_type' and  post['booked_date']:
                booked_date = datetime.strptime(post.get('booked_date' or ''), '%m/%d/%Y').date()
                # booked_date = datetime.strptime(post.get('booked_date' or ''), '%Y-%m-%d').date()
                support.sudo().write({'custom_booked_date': booked_date})
                support.create_work_order()
                
                query = """
                    SELECT
                        PT.id
                    FROM
                        project_task PT
                    WHERE
                        PT.ticket_id = %s
                """
                args = [support.id]
                request._cr.execute(query, tuple(args))
                task_ids = request._cr.dictfetchall()
                task_list = []
                for task in task_ids:
                    task_list.append(task.get('id'))
                task_ids = request.env['project.task'].sudo().browse(task_list)
#                 task_id = request.env['project.task'].sudo().search([('ticket_id', '=', support.id)])
                task_ids.sudo().write({
                    'planned_hours': 3.0,
                    'date_deadline': booked_date,
                    'custom_booked_date': booked_date,
                    'is_book_maintenance': True,
                    'company_id': request.env.user.company_id.id,
                })

            attachment_list = request.httprequest.files.getlist('attachment')
            for image in attachment_list:
                if post.get('attachment'):
                    attachments = {
                               'res_name': image.filename,
                               'res_model': 'machine.repair.support',
                               'res_id': support,
                               'datas': base64.encodestring(image.read()),
                               'type': 'binary',
                               'datas_fname': image.filename,
                               'name': image.filename,
                           }
                    attachment_obj = http.request.env['ir.attachment']
                    attach = attachment_obj.sudo().create(attachments)
            if len(attachment_list) > 0:
                group_msg = _('Customer has sent %s attachments to this machine repair ticket. Name of attachments are: ') % (len(attachment_list))
                for attach in attachment_list:
                    group_msg = group_msg + '\n' + attach.filename
                group_msg = group_msg + '\n'  +  '. You can see top attachment menu to download attachments.'
                support.sudo().message_post(body=group_msg,message_type='comment')
            template = http.request.env.ref('machine_repair_management_extend.website_throw_create_machine_repair_request')
            template.sudo().send_mail(support.id)
            return request.render('machine_repair_management.thanks_mail_send', values)
        else:
            return request.render('machine_repair_management.support_invalid',{'user':request.env.user})

    @http.route(['/machine_repair_management/update_product_info'], type='json', auth="public", website=True)
    def UpdateProductInfo(self, dict=[], **post):
        data = {}
        if dict and dict.get('product_id'):
            product_id = request.env['product.product'].sudo().browse(int(dict.get('product_id')))
            data = {
                'brand': str(product_id.brand),
                'model': str(product_id.model),
                'year': str(product_id.year),
            }
        return data
