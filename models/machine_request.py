# -*- coding: utf-8 -*

from odoo import models, fields, api

class MachineRepairSupport(models.Model):
    _inherit = 'machine.repair.support'

    @api.model_cr
    def init(self):
        ticket_ids = self.env['machine.repair.support'].sudo().search([])
        for ticket_id in ticket_ids:
            ticket_id._compute_allow_contact_ids()

    @api.multi
    @api.depends('partner_id')
    def _compute_allow_contact_ids(self):
        for rec in self:
            partner_id = rec.partner_id
            parent_ids = partner_id
            
            #when partner is company
            if partner_id.company_type == 'company':
                company_child_ids = partner_id.child_ids.filtered(lambda j : j.type=='contact')
                location_ids = partner_id.child_ids.filtered(lambda m : m.type=='other')
                location_child_ids = location_ids.mapped("child_ids").filtered(lambda t : t.type=='contact')
                for location_child_id in location_child_ids:
                    if location_child_id.id in rec.custom_location_id.child_ids.ids:
                        parent_ids += location_child_id
                parent_ids += company_child_ids

            #when partner is Admin User
            if partner_id.parent_id and partner_id.parent_id.company_type == 'company':
                parent_ids += partner_id.parent_id

            #when partner is location
            if partner_id.type == 'other':
                other_child_ids = partner_id.child_ids.filtered(lambda j : j.type=='contact')
                parent_ids += other_child_ids
                if partner_id.parent_id and partner_id.parent_id.company_type == 'company':
                    company_child_ids = partner_id.parent_id.child_ids.filtered(lambda j : j.type=='contact')
                    company_child_ids += partner_id.parent_id
                    parent_ids += company_child_ids

            #when partner is Normal User
            if partner_id.parent_id and partner_id.parent_id.type == 'other' and partner_id.parent_id.parent_id.company_type == 'company' :
                parent_comany_child_ids = partner_id.parent_id.parent_id.child_ids.filtered(lambda a : a.type=='contact')
                parent_ids += parent_comany_child_ids
                parent_ids += partner_id.parent_id.child_ids.filtered(lambda j : j.type=='contact')
                parent_ids += partner_id.parent_id.parent_id

            rec.allow_contact_ids = parent_ids.filtered(lambda x: x.type != 'other')

    contract_number = fields.Char(
        string='Contract Number',
    )
    custom_location_id = fields.Many2one(
        'res.partner', 
        string='Location Name', 
    )
    company_serial_number = fields.Char(
        string='Company Serial Number',
    )
    custom_booked_date = fields.Date(
        string="Visit Date"
    )
    custom_machine_type = fields.Selection([
        ('repair_type', 'Repair'),
        ('booked_type', 'Booked Maintenance')],
        sting="Service"
    )
    allow_contact_ids = fields.Many2many(
        'res.partner',
        'res_partner_allow_contact_rel',
        'allow_contact_id',
        'partner_id',
        string="Allow Contacts",
        compute = "_compute_allow_contact_ids",
        store=True
    )
    custome_client_user_id = fields.Many2one(
        'res.users',
        string="Ticket Created User",
        readonly = True,
        track_visibility='always'
    )

    @api.model
    def create(self, vals):
        if vals.get('custome_client_user_id', False):
            client_user_id = self.env['res.users'].browse(int(vals.get('custome_client_user_id')))
            if client_user_id:
                vals.update({'company_id': client_user_id.company_id.id})
        else:
            vals.update({'custome_client_user_id': self.env.user.id})
        return super(MachineRepairSupport, self).create(vals)

#     @api.model
#     def search(self, args, offset=0, limit=0, order=None, count=False):
#         if self.env.user.has_group('base.group_portal'):
#             if not self.env.user.partner_id.parent_id or self.env.user.partner_id.type == 'other':
#                 args = ['|', ('partner_id', 'child_of', self.env.user.partner_id.child_ids.ids), ('partner_id', '=', self.env.user.partner_id.id)]
#             elif self.env.user.partner_id.parent_id and self.env.user.partner_id.type != 'other' and self.env.user.partner_id.parent_id.parent_id:
#                 args = ['|','|',('partner_id', '=', self.env.user.partner_id.parent_id.id),('partner_id', '=', self.env.user.partner_id.parent_id.parent_id.id),('partner_id', '=', self.env.user.partner_id.id)]
#             else:
#                 pass
#         return super(MachineRepairSupport, self).search(args, offset, limit, order, count)

    @api.onchange('project_id')
    def onchange_project_id(self):
        self.ensure_one()
        res = {}
        self.contract_number = self.project_id.analytic_account_id.name
        parttner_ids = []
        if self.project_id and self.project_id.partner_id:
            parttner_ids.append(self.project_id.partner_id.id)
        res['domain'] = {
            'custom_location_id': [('id', 'child_of', parttner_ids), ('type', '=', 'other')],
        }
        return res

    @api.onchange('product_id')
    def onchange_product_id(self):
        self.ensure_one()
        self.company_serial_number = self.product_id.barcode

    @api.multi
    @api.onchange('product_category')
    def product_id_change(self):
        self.ensure_one()
        res = super(MachineRepairSupport, self).product_id_change()
        res['domain'] = {'product_id': [('contract_project_id', '=', self.project_id.id), ('custom_location_id', '=', self.custom_location_id.id)],}
        return res

    @api.onchange('custom_location_id')
    def onchange_custom_location_id(self):
        self.ensure_one()
        res = {}
        res['domain'] = {
            'product_id': [('contract_project_id', '=', self.project_id.id), ('custom_location_id', '=', self.custom_location_id.id)],
        }
        return res
